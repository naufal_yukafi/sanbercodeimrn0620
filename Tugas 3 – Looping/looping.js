//soal 1
console.log("===================Soal 1====================")
console.log("LOOPING PERTAMA")
var nilaiAwal = 1;
while (nilaiAwal <= 20) {
    if (nilaiAwal % 2 == 0) {
        console.log(nilaiAwal + " - I love coding")
    }
    nilaiAwal++;
}


console.log("LOOPING KEDUA")
var nilaiAwal2 = 20;
while (nilaiAwal2 > 0) {
    if (nilaiAwal2 % 2 == 0) {
        console.log(nilaiAwal2 + " - I will become a mobile developer")
    }
    nilaiAwal2--;
}


//soal 2
console.log("===================Soal 2====================")
var string1 = "Santai";
var string2 = "Berkualitas";
var string3 = "I Love Coding";
for (let i = 1; i <= 20; i++) {
    var genap = (i % 2 == 0)
    if (genap) { //genap
        console.log(i + " - " + string2)
    } else if (i % 3 == 0 && !genap) { //kelipatan 3 dan ganjil
        console.log(i + " - " + string3)
    } else if (!genap) { //ganjil
        console.log(i + " - " + string1)
    }
}


//soal 3
console.log("===================Soal 3====================")
var pagar = ""
for (var i = 1; i < 5; i++) {
    for (var n = 1; n < 9; n++) {
        pagar += "#";
    }
    console.log(pagar)
    console.log(pagar = "")
}


//soal 4
console.log("===================Soal 4====================")
var pagar2 = " "
for (let m = 0; m < 7; m++) {
    pagar2 += "#";
    console.log(pagar2);
}

//soal 5
console.log("===================Soal 5====================")
var pagar5 = ""
for (var q = 1; q < 9; q++) {
    var genap2 = q % 2 == 0;
    if (!genap2) {
        for (var w = 1; w < 9; w++) {
            var genap = w % 2 == 0;
            if (genap) {
                pagar += "#";
            } else {
                pagar += " ";
            }
        }
    } else if (genap2) {
        for (var w = 1; w < 9; w++) {
            var genap = w % 2 == 1;
            if (genap) {
                pagar += "#";
            } else {
                pagar += " ";
            }
        }
    }
    console.log(pagar)
    console.log(pagar = "")
}


