//soal 1
console.log("============== soal 1 ================")
function arrayToObject(people) {
    if (people == 0) {
        return console.log("data kosong")
    } else {
        for (var i = 0; i < people.length; i++) {
            var now = new Date()
            var thisYear = now.getFullYear()
            var ageYear = people[i][3]
            function age() {
                if (ageYear == null || ageYear > thisYear) {
                    return "Invalid Birth Year"
                } else {
                    return thisYear - ageYear
                }
            }
            var tampung = {
                firstName: people[i][0],
                lastName: people[i][1],
                gender: people[i][2],
                age: age()
            }
            console.log((i + 1) + ". " + people[i][0] + " " + people[i][1] + ": ", tampung)
            console.log()
        }
        return tampung
    }

}

var people = [["Bruce", "Banner", "male", 1975], ["Natasha", "Romanoff", "female"]]
arrayToObject(people)

var people2 = [["Tony", "Stark", "male", 1980], ["Pepper", "Pots", "female", 2023]]
arrayToObject(people2)

arrayToObject([]) // ""

console.log()

//soal 2
console.log("============== soal 2 ================")
function shoppingTime(memberId, money) {

    var produk = [["Sepatu Stacattu", 1500000], ["Baju Zoro", 500000], ["Baju H&N", 250000], ["Sweater Uniklooh", 175000], ["Casing Handphone", 50000]]
    var belanja = []
    var data = []
    if (memberId === "" || memberId == null) {
        return "Mohon maaf, toko X hanya berlaku untuk member saja"
    } else {
        if (money < 50000) {
            return "Mohon maaf, uang tidak cukup"
        } else {
            var changeMoney = money
            for (var n = 0; n < produk.length; n++) {
                if (changeMoney >= produk[n][1]) {
                    var changeMoney = changeMoney - produk[n][1]
                    var simpanProduk = produk[n][0]
                    belanja.push(simpanProduk)
                }
            }
            var obj = {
                memberId: memberId,
                money: money,
                listPurchased: belanja,
                changeMoney: changeMoney
            }
            data.push(obj)
        }
        return data

    }


}

console.log(shoppingTime('1820RzKrnWn08', 2475000));
console.log(shoppingTime('82Ku8Ma742', 170000));
console.log(shoppingTime('', 2475000));
console.log(shoppingTime('234JdhweRxa53', 15000));
console.log(shoppingTime());
//soal 3
console.log("============== soal 3 ================")
function naikAngkot(listPenumpang) {
    var rute = ["A", "B", "C", "D", "E", "F"];
    var tampung2 = []
    for (var j = 0; j < listPenumpang.length; j++) {
        function totalBayar() {
            var naikDari = listPenumpang[j][1]
            var tujuan = listPenumpang[j][2]
            return bayar = (rute.indexOf(tujuan) - rute.indexOf(naikDari)) * 2000
        }
        var dataPenumpang = {
            penumpang: listPenumpang[j][0],
            naikDari: listPenumpang[j][1],
            tujuan: listPenumpang[j][2],
            bayar: totalBayar()
        }
        tampung2.push(dataPenumpang)
    }
    return tampung2
}

console.log(naikAngkot([['Dimitri', 'B', 'F'], ['Icha', 'A', 'B']]));
console.log(naikAngkot([])); //[]