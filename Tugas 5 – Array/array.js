//soal 1
console.log("============== Soal 1 ====================")
function range(startNum, finishNum) {
    if (!startNum || !finishNum) {
        return -1
    } else {
        var number = []
        if (startNum < finishNum) {
            for (var i = startNum; i <= finishNum; i++) {
                number.push(i)
            }
            number.sort(function (a, b) { return a - b })
        } else {
            for (var j = startNum; j >= finishNum; j--) {
                number.push(j)
            }
            number.sort(function (a, b) { return b - a })
        }
        return number
    }
}
console.log(range(1, 10))
console.log(range(1))
console.log(range(11, 18))
console.log(range(54, 50))
console.log(range())

// //soal 2
console.log("============== Soal 2 ====================")
function rangeWithStep(startNum, finishNum, step) {
    if (!startNum || !finishNum) {
        return -1
    } else {
        var number2 = []
        if (startNum < finishNum) {
            for (var i = startNum; i <= finishNum; i += step) {
                number2.push(i)
            }
            number2.sort(function (a, b) { return a - b })
        } else {
            for (var j = startNum; j >= finishNum; j -= step) {
                number2.push(j)
            }
            number2.sort(function (a, b) { return b - a })
        }
        return number2
    }
}
console.log(rangeWithStep(1, 10, 2))
console.log(rangeWithStep(11, 23, 3))
console.log(rangeWithStep(5, 2, 1))
console.log(rangeWithStep(29, 2, 4))

// soal 3
console.log("============== Soal 3 ====================")
function sum(startNum3, finishNum3, step3 = 1) {
    var number3 = 0;
    var arrFunction = rangeWithStep(startNum3, finishNum3, step3)
    if (startNum3 && !finishNum3) {
        number3 = startNum3
    } else if ("") {
        number3 = 0
    } else {
        for (i = 0; i < arrFunction.length; i++) {
            number3 += arrFunction[i]
        }
    }
    return number3
}
console.log(sum(1, 10))
console.log(sum(5, 50, 2))
console.log(sum(15, 10))
console.log(sum(20, 10, 2))
console.log(sum(1))
console.log(sum())
console.log("============== Soal 4 ====================")
function dataHandling() {
    var input = [
        ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
        ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
        ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
        ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
    ]
    var argument = ""
    for (var m = 0; m < input.length; m++) {
        argument += `
            Nomor ID: ${input[m][0]}
            Nama Lengkap: ${input[m][1]}
            TTL: ${input[m][2]} ${input[m][3]}
            Hobi: ${input[m][4]}
        `
    }
    return argument
}
console.log(dataHandling())

console.log("============== Soal 5 ====================")
function balikKata(nama) {
    var kalimat = ""
    for (var m = (nama.length - 1); m >= 0; m--) {
        kalimat += nama[m]
    }
    return kalimat
}
console.log(balikKata("Kasur Rusak"))
console.log(balikKata("SanberCode"))
console.log(balikKata("Haji Ijah"))
console.log(balikKata("racecar"))
console.log(balikKata("I am Sanbers"))

console.log("============== Soal 6 ====================")
// ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
function dataHandling2(data) {
    data.splice(1, 1, "Roman Alamsyah Elsharawy")
    data.splice(2, 1, "Provinsi Bandar Lampung")
    data.splice(4, 4, "Pria")
    data.splice(6, 6, "SMA Internasional Metro")
    console.log(data)
    thn = data[3].split("/")
    switch (thn[1]) {
        case '01': { console.log("Januari"); break; }
        case '02': { console.log("Februari"); break; }
        case '03': { console.log("Maret"); break; }
        case '04': { console.log("April"); break; }
        case '05': { console.log("Mei"); break; }
        case '06': { console.log("Juni"); break; }
        case '07': { console.log("Juli"); break; }
        case '08': { console.log("Agustus"); break; }
        case '09': { console.log("September"); break; }
        case '10': { console.log("Oktober"); break; }
        case '11': { console.log("November"); break; }
        case '12': { console.log("Desember"); break; }
        default: { console.log("Inputan melebihi batas"); break; }
    }
    thn.sort(function (a, b) { return b - a })
    console.log(thn)
    arr = data[3].split("/")
    arrJoin = arr.join("-")
    console.log(arrJoin)
    if (data[1].length > 15) {
        nameD = data[1].slice(0, 15)
        console.log(nameD)
    } else {
        console.log("nama tidak melebihi 15 karakter")
    }

}
var data = ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"]
dataHandling2(data)