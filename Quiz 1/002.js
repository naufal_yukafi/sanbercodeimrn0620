//soal A
console.log("================== Soal A =======================")
function AscendingTen(Number) {
    if (!Number) {
        return -1
    } else {
        var data = Number + " "
        for (var i = 1; i < 10; i++) {
            data += (Number += 1) + " "
        }
        return data
    }
}
console.log(AscendingTen(11)) // 11 12 13 14 15 16 17 18 19 20
console.log(AscendingTen(21)) // 21 22 23 24 25 26 27 28 29 30
console.log(AscendingTen()) // -1

//Soal B
console.log("================== Soal B =======================")
function DescendingTen(Number2) {
    if (!Number2) {
        return -1
    } else {
        var data2 = Number2 + " "
        for (var j = 10; j > 1; j--) {
            data2 += (Number2 -= 1) + " "
        }
        return data2
    }
}
console.log(DescendingTen(100)) // 100 99 98 97 96 95 94 93 92 91
console.log(DescendingTen(10)) // 10 9 8 7 6 5 4 3 2 1
console.log(DescendingTen()) // -1

//Soal C
console.log("================== Soal C =======================")
function ConditionalAscDesc(reference, check) {
    var data1 = ""
    var dataAsc = AscendingTen(reference)
    var dataDesc = DescendingTen(reference)
    for (var k = 0; k < 1; k++) {
        var genap = k % 2 == 0
        if (!reference || !check) {
            return -1
        } else if (check == genap) {
            //ascending
            return data1 += dataAsc
        } else if (check != genap) {
            //descending
            return data1 += dataDesc
        }
    }
}
console.log(ConditionalAscDesc(20, 8)) // 20 19 18 17 16 15 14 13 12 11
console.log(ConditionalAscDesc(81, 1)) // 81 82 83 84 85 86 87 88 89 90
console.log(ConditionalAscDesc(31)) // -1
console.log(ConditionalAscDesc()) // -1

//Soal D
console.log("================== Soal D =======================")
function ularTangga() {
    var angka = 100
    var tampungAngka = ""

    for (var p = 1; p < 10; p++) {
        var genap3 = p % 2 == 0
        for (var q = 1; q < 10; q++) {
            if (genap3) {
                tampungAngka += (angka -= 1) + " "
            } else {

            }

        }
        console.log(tampungAngka)
        console.log(tampungAngka = "")
    }
}
// data = ""
// angka = 100
// for (var p = 1; p < 10; p++) {
//     for (var q = 1; q < 10; q++) {
//         var genap = q % 2 == 0
//         // if (genap) {
//         data += (angka -= 1) + " "
//         // }

//     }
//     console.log(data)
//     console.log(data = "")
// }

console.log(ularTangga())
