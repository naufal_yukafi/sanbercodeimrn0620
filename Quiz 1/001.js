//soal A
console.log("================== Soal A =======================")
function balikString(stringBaru) {
    var kalimat = ""
    for (var m = (stringBaru.length - 1); m >= 0; m--) {
        kalimat += stringBaru[m]
    }
    return kalimat
}
console.log(balikString("abcde")) // edcba
console.log(balikString("rusak")) // kasur
console.log(balikString("racecar")) // racecar
console.log(balikString("haji")) // ijah

//soal B
console.log("================== Soal B =======================")
function palindrome(stringBaru) {
    var kalimat = ""
    for (var m = (stringBaru.length - 1); m >= 0; m--) {
        kalimat += stringBaru[m]
    }
    if (stringBaru == kalimat) {
        return true;
    } else {
        return false;
    }
}
console.log(palindrome("kasur rusak")) // true
console.log(palindrome("haji ijah")) // true
console.log(palindrome("nabasan")) // false
console.log(palindrome("nababan")) // true
console.log(palindrome("jakarta")) // false

//Soal C
console.log("================== Soal C =======================")
function bandingkan(number, bilPos) {
    var kosong = 0
    if (number > bilPos) {
        return number
    } else if (bilPos > number) {
        return bilPos
    } else if (number == bilPos || number == "") {
        return -1
    }
    else if (number || bilpos) {
        return 1
    }
}
console.log(bandingkan(10, 15)); // 15
console.log(bandingkan(12, 12)); // -1
console.log(bandingkan(-1, 10)); // -1 
console.log(bandingkan(112, 121));// 121
console.log(bandingkan(1)); // 1
console.log(bandingkan()); // -1
console.log(bandingkan("15", "18")) // 18
