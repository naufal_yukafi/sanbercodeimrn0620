import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native'

export default class SignUpScreen extends Component {

    render() {

        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.containerTitle}>
                        <Image source={require("./Images/logo.png")} style={styles.logo} />
                        <Text style={styles.title}>Lirik Programer</Text>
                        <TextInput style={styles.inputUser} />
                        <TextInput style={styles.inputUser} />
                        <TextInput style={styles.inputUser} />
                        <TouchableOpacity style={styles.signup}>
                            <Text style={styles.subText}>SIGN UP</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.login}>
                            <Text style={styles.subText}>LOGIN</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        flex: 1,
        backgroundColor: '#040404'
    },
    logo: {
        marginLeft: 62
    },
    title: {
        fontFamily: "Roboto",
        fontSize: 30,
        fontWeight: "bold",
        textAlign: "center",
        color: "#FDCD32",
    },
    containerTitle: {
        marginTop: "8%",
        margin: 20,
    },
    inputUser: {
        paddingHorizontal: 20,
        height: 43,
        backgroundColor: '#1A1A1A',
        color: '#fff',
        borderRadius: 8,
        fontSize: 16,
        marginTop: 30,

    },
    signup: {
        marginTop: 30,
        height: 43,
        borderRadius: 8,
        backgroundColor: '#FDCD32',

    },
    login: {
        marginTop: 30,
        height: 43,
        borderRadius: 8,
        backgroundColor: '#C4C4C4',
    },
    subText: {
        paddingTop: 12,
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,
        textAlign: 'center'
    },
    signUp: {
        textAlign: 'center',
        padding: 15,
        flex: 1
    },
    footer: {
        textAlign: 'center',
        fontSize: 16,
        color: '#fff',
    }

});