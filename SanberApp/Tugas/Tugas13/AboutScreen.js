import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class AboutScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.image}>
                    <Image source={require('./Images/profile.png')} style={{ resizeMode: "cover" }} />
                </View>
                <View style={styles.body}>
                    <Text style={styles.name}>Naufal Yukafi Ridlo</Text>
                    <Text style={styles.desc}>
                        Hello, Welcome to Lyrics Programmer. This application is intended so that companies can glance at user programing skills. And If you want to know more about me, you can see below:
                    </Text>
                </View>
                <View style={styles.portfolio}>
                    <View>
                        <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>Social Media</Text>
                        <View style={styles.icon}>
                            <Image source={require('./Images/facebook.png')} />
                            <Text style={{ color: 'white', marginLeft: 5 }} >@Naufal_yukafi</Text>
                        </View>
                        <View style={styles.icon}>
                            <Image source={require('./Images/instagram.png')} style={styles.subicon} />
                            <Text style={{ color: 'white', marginTop: 20, marginLeft: 5 }} >@yukafi.dev</Text>
                        </View>
                    </View>
                    <View>
                        <Text style={{ color: '#fff', fontSize: 20, fontWeight: 'bold' }}>Portfolio</Text>
                        <View style={styles.icon}>
                            <Image source={require('./Images/github.png')} />
                            <Text style={{ color: 'white', marginLeft: 5 }} >@Naufal_yukafi</Text>
                        </View>
                        <View style={styles.icon}>
                            <Image source={require('./Images/gitlab.png')} style={styles.subicon} />
                            <Text style={{ color: 'white', marginTop: 20, marginLeft: 5 }} >@Naufal_yukafi</Text>
                        </View>

                    </View>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        flex: 1,
        backgroundColor: '#040404'
    },
    image: {
        height: 275
    },
    body: {
        color: 'white'
    },
    name: {
        color: 'white',
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        color: '#fff',
        marginTop: 28
    },
    desc: {
        fontSize: 14,
        color: '#fff',
        marginTop: 20,
        paddingHorizontal: 25
    },
    portfolio: {
        marginTop: 18,
        height: 200,
        paddingHorizontal: 25,
        flexDirection: 'row',
        // backgroundColor: 'yellow',
        // textAlign: 'center',
        justifyContent: 'space-between'
    },
    icon: {
        marginTop: 10,
        flexDirection: 'row'
    },
    subicon: {
        marginTop: 20
    },
    userName: {
        color: 'white',
        marginTop: 10
    },



});