import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    ScrollView,
} from 'react-native'

import Icon from 'react-native-vector-icons/MaterialIcons';

export default class BerandaScreen extends Component {
    render() {
        return (
            <View style={styles.container}>
                <View style={styles.search}>
                    <TextInput
                        style={styles.inputUser}
                    />
                </View>
                <View style={styles.containerBody}>
                    <ScrollView>
                        <View style={styles.body}>
                            <View style={styles.bodyImage}>
                                <Image source={require('./Images/user1.png')} />
                            </View>
                            <View style={styles.bodyTitle}>
                                <Text style={styles.bodyName}>Sukiem</Text>
                                <Text style={styles.bodySkill}>Front End Enginering</Text>
                                <Text style={styles.bodyYear}> 3 Years</Text>
                                <Text style={styles.bodyLink}> Lirik... </Text>
                            </View>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.bodyImage}>
                                <Image source={require('./Images/user2.png')} />
                            </View>
                            <View style={styles.bodyTitle}>
                                <Text style={styles.bodyName}>Bara</Text>
                                <Text style={styles.bodySkill}>Front End Enginering</Text>
                                <Text style={styles.bodyYear}> 4 Years</Text>
                                <Text style={styles.bodyLink}> Lirik... </Text>
                            </View>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.bodyImage}>
                                <Image source={require('./Images/user2.png')} />
                            </View>
                            <View style={styles.bodyTitle}>
                                <Text style={styles.bodyName}>Bara</Text>
                                <Text style={styles.bodySkill}>Front End Enginering</Text>
                                <Text style={styles.bodyYear}> 4 Years</Text>
                                <Text style={styles.bodyLink}> Lirik... </Text>
                            </View>
                        </View>

                        <View style={styles.body}>
                            <View style={styles.bodyImage}>
                                <Image source={require('./Images/user2.png')} />
                            </View>
                            <View style={styles.bodyTitle}>
                                <Text style={styles.bodyName}>Bara</Text>
                                <Text style={styles.bodySkill}>Front End Enginering</Text>
                                <Text style={styles.bodyYear}> 4 Years</Text>
                                <Text style={styles.bodyLink}> Lirik... </Text>
                            </View>
                        </View>
                    </ScrollView>
                </View>

                <View style={styles.tabBar}>
                    <TouchableOpacity style={styles.tabItem}>
                        <Image source={require('./Images/home.png')} />
                        <Text style={styles.tabTitle}>HOME</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={styles.tabItem}>
                        <Image source={require('./Images/about.png')} />
                        <Text style={styles.tabTitle}>ABOUT</Text>
                    </TouchableOpacity>
                </View>
            </View >
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        flex: 1,
        backgroundColor: '#040404'
    },
    search: {
        // height: 60,
        // backgroundColor: 'yellow'
    },
    containerBody: {
        height: 200,
        // backgroundColor: 'blue',
        flex: 1
    },
    inputUser: {
        height: 48,
        backgroundColor: '#2C2C2C',
        borderRadius: 20,
        color: '#eee',
        paddingHorizontal: 25,
        marginTop: 30,
        marginRight: 40,
        marginLeft: 40
    },
    tabItem: {
        alignItems: "center",
        justifyContent: 'center'
    },
    tabBar: {
        backgroundColor: '#2C2C2C',
        height: 60,
        borderTopWidth: 0.5,
        // borderColor: '#E5E5E5',
        flexDirection: 'row',
        justifyContent: 'space-evenly'
    },
    tabTitle: {
        fontSize: 12,
        // color: "#3c3c3c",
        paddingTop: 5,
        color: 'white'
    },
    body: {
        height: 165,
        // backgroundColor: 'white',
        marginTop: 50,
        padding: 10,
        paddingHorizontal: 15,
        flexDirection: 'row'
    },
    bodyTitle: {
        marginLeft: 10,
        padding: 20
        // alignItems: 'center'
    },
    bodyName: {
        fontWeight: 'bold',
        fontSize: 20,
        color: "#fff"
    },
    bodySkill: {
        color: '#fff',
        paddingTop: 10
    },
    bodyYear: {
        color: '#fff',
        paddingTop: 10
    },
    bodyLink: {
        color: '#FDCD32',
        paddingTop: 10,

    }


});