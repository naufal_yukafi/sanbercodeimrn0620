import React, { Component } from 'react'
import {
    View,
    StyleSheet,
    Text,
    TextInput,
    Image,
    TouchableOpacity,
    ScrollView
} from 'react-native'
export default class LoginScreen extends Component {
    static navigationOptions = {
        // Sets the title of the Header
        title: 'LoginScreen',
    };
    render() {

        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.containerTitle}>
                        <Image source={require("./Images/logo.png")} style={styles.logo} />
                        <Text style={styles.title}>Lirik Programer</Text>
                        <TextInput
                            style={styles.inputUser}
                        />
                        <TextInput style={styles.inputUser} />
                        <TouchableOpacity style={styles.login}
                            onPress={() => { this.props.navigation.navigate('LoginScreen') }}
                        >
                            <Text style={styles.subText}>Login</Text>
                        </TouchableOpacity>
                    </View>
                </ScrollView>
                <ScrollView>
                    <View style={styles.signUp}>
                        <Text style={styles.footer}>Don’t have an account? <Text style={{ color: '#FDCD32', fontSize: 16, fontWeight: 'bold' }}>SignUp here</Text></Text>
                    </View>
                </ScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 25,
        flex: 1,
        backgroundColor: '#040404'
    },
    logo: {
        marginLeft: 62
    },
    title: {
        fontFamily: "Roboto",
        fontSize: 30,
        fontWeight: "bold",
        textAlign: "center",
        color: "#FDCD32",
    },
    containerTitle: {
        marginTop: "8%",
        margin: 20,
    },
    inputUser: {
        paddingHorizontal: 20,
        height: 43,
        backgroundColor: '#1A1A1A',
        color: '#fff',
        borderRadius: 8,
        fontSize: 16,
        marginTop: 30,

    },
    login: {
        marginTop: 30,
        height: 43,
        borderRadius: 8,
        backgroundColor: '#FDCD32',
        // flex: 1
    },
    subText: {
        paddingTop: 12,
        fontWeight: 'bold',
        fontSize: 16,
        lineHeight: 19,
        textAlign: 'center'
    },
    signUp: {
        textAlign: 'center',
        padding: 15,
        // flex: 1
    },
    footer: {
        // backgroundColor: 'red',
        textAlign: 'center',
        fontSize: 16,
        color: '#fff',
    }

});