var readBooks = require('./callback.js')
var books = [
    { name: "LOTR", timeSpent: 3000 },
    { name: "Fidas", timeSpent: 2000 },
    { name: "Kalkulus", timeSpent: 4000 }
]
var waktu = 10000
var membaca = function (time, i) {
    readBooks(time, books[i], function (check) {
        if (i != books.length - 1 && check >= books[i + 1].timeSpent) {
            membaca(check, i + 1)
        }
    })
}
membaca(waktu, 0)