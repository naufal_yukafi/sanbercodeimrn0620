//soal 1
console.log("================ Soal 1 =======================")
function teriak() {
    var halo = "Halo Sanbers!";
    return halo;
}
console.log(teriak())

//soal 2
console.log("================ Soal 2 =======================")
function kalikan(numb1, numb2) {
    return numb1 * numb2
}
var numb1 = 12
var numb2 = 4
var hasilKali = kalikan(numb1, numb2)
console.log(hasilKali)

//soal 3
console.log("================ Soal 3 =======================")
function introduce(name, age, address, hobby) {
    var bio = `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
    return bio
}
var name = "Agus"
var age = 30
var address = "Jln. Malioboro, Yogyakarta"
var hobby = "Gaming"

var perkenalan = introduce(name, age, address, hobby)
console.log(perkenalan)