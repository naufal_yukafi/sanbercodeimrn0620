//soal 1
console.log("================= Soal 1 ==================")
const golden = goldenFunction = () => {
    console.log("this is golden!")
}
golden()

//soal 2
console.log("================= Soal 2 ==================")
const newFunction = literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: () => {
            console.log(`${firstName} ${lastName}`)
            return
        }
    }
}
newFunction("William", "Imoh").fullName()

//soal 3
console.log("================= Soal 3 ==================")
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    Occupation: "Deve-wizard Avocado", spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, Occupation } = newObject
console.log(firstName, lastName, destination, Occupation)

//soal 4
console.log("================= Soal 4 ==================")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
console.log(combined)

//soal 5
console.log("================= Soal 5 ==================")
const planet = "earth"
const view = "glass"

var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
console.log(before)
